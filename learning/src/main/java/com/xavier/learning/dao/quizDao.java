package com.xavier.learning.dao;

import com.xavier.learning.entity.Quiz;

import java.util.List;

public interface quizDao {
    Quiz getQuizById(Integer id);
    public List<Quiz> getQuizList();
    public int add(Quiz user);
    public int update(Integer id, Quiz quiz);
    public int delete(Integer id);
}

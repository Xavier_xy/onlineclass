package com.xavier.learning.Quiz;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.xavier.learning.Service.ProfileService;
import com.xavier.learning.Service.QuizService;
import com.xavier.learning.WebSocket.WebSocketServer;
import com.xavier.learning.entity.Message;
import com.xavier.learning.entity.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.IOException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by xiaoy on 2019/3/19 22:18
 */
public class Progress {
    @Autowired
    private static JdbcTemplate jdbcTemplate;
    private static Map<String,List<String>> QuizStore=new HashMap<>();
    private static Map<String,List<String>> CheckStore=new HashMap<>();
    private static Map<String,Long> TimeStore=new HashMap<>();
    private static Map<String,Long> AnswerTime=new HashMap<>();
    private static Map<String,Long> CheckTinme1=new HashMap<>();
    private static Map<String,Long> CheckTinme2=new HashMap<>();
    private static Map<String,List<String>> IDtoQuiz=new HashMap<>();
    private static Map<String,List<String>> Mark=new HashMap<>();
    private static Map<String,List<String>> SchoolAndMajor=new HashMap<>();
    private static Map<String,List<Float>> Score=new HashMap<>();
    private static Map<String,Integer> CheckCounter=new HashMap<>();
    private static Map<String,Integer> Counter=new HashMap<>();
    private static Map<String,Integer> Counter_check=new HashMap<>();
    private static Map<String,Boolean> IsCheck=new HashMap<>();
    private static Map<String,String> Sender=new HashMap<>();
    private static Map<String,Map<String,String>> Reason=new HashMap<>();
    public static void SolveMessage(String message, QuizService quizService){
        try {
            Message message1 = JSON.parseObject(message, Message.class);
            JSONObject data = JSON.parseObject(message1.getData());
            Long currentTime = System.currentTimeMillis();
                if (message1.getCode() == 1001) {//收到提问消息
                    String uuid = data.getString("questionid");
                    List<String> Respondent = new ArrayList<>();
                    List<String> Check = new ArrayList<>();
                    List<String> QuizAndAnswer = new ArrayList<>();
                    Map<String,String> reason = new HashMap<>();
                    String Question = data.getString("question");
                    String school = data.getString("question");
                    String major = data.getString("question");
                    List<String> sam = new ArrayList<>();
                    sam.add(school);
                    sam.add(major);
                    QuizAndAnswer.add(Question);//存储问题
                    List<String> mark = new ArrayList<>();
                    mark.add(data.getString("mark1"));
                    mark.add(data.getString("mark2"));
                    if (!QuizStore.containsKey(Question)) {
                        Long startTime = System.currentTimeMillis();
                        IDtoQuiz.put(uuid, QuizAndAnswer);//存储UUID到问题的映射
                        QuizStore.put(uuid, Respondent);//存储问题的回答者和审核者
                        CheckStore.put(uuid, Check);//存储问题的已审核的人
                        CheckCounter.put(uuid, 0);//存储计数
                        Counter.put(uuid, 0);
                        Counter_check.put(uuid, 0);
                        IsCheck.put(uuid, false);
                        Sender.put(uuid, message1.getId());//存储发送人
                        TimeStore.put(uuid, startTime);//存储开始时间
                        AnswerTime.put(uuid,0L);
                        CheckTinme1.put(uuid,0L);
                        CheckTinme2.put(uuid,0L);
                        Mark.put(uuid,mark);
                        SchoolAndMajor.put(uuid,sam);
                        Reason.put(uuid,reason);
                    }
                    Response("2001", "发送成功", message1.getId());
                    JSONObject back = new JSONObject();
                    back.put("question", data.getString("question"));
                    back.put("questionID", uuid);
                    back.put("school", school);
                    back.put("major", major);
                    SendMessage("搜寻回答", "1002", back, message1.getId());
                }
                else {
                    String QuizID = data.get("QuizID").toString();
                    if(IDtoQuiz.containsKey(QuizID)) {
                        if (message1.getCode() == 1003) {//收到用户接受消息
                            int counter = Counter.get(QuizID);
                            List<String> Respondent = QuizStore.get(QuizID);
                            if (counter < 1 && !Respondent.contains(message1.getId())) {
                                Response("2003", "接受成功，请等待结果", message1.getId());
                                Counter.replace(QuizID, ++counter);
                                Respondent.add(message1.getId());//收集接受用户信息
                            }
                            if (!Respondent.contains(message1.getId())) {
                                Counter.replace(QuizID, ++counter);
                                //Response("2003", "请勿重复接受!", message1.getId());
                            }
                            if (counter == 1) {
                                JSONObject back = new JSONObject();
                                back.put("questionID", QuizID);
                                back.put("mark1", Mark.get(QuizID).get(0));
                                back.put("mark2", Mark.get(QuizID).get(1));
                                Response("1004", "请回答", back.toJSONString(), Respondent.get(0));
                                AnswerTime.put(QuizID, currentTime);
                            }
                            if (counter > 1) {
                                Response("2004", "手速慢了哟!", message1.getId());
                            }
                        }
                        if (message1.getCode() == 1006) {//收到回答完毕消息
                            List<String> Respondent = QuizStore.get(QuizID);
                            List<String> QuizAndAnswer = IDtoQuiz.get(QuizID);
                            AnswerTime.replace(QuizID, 1L);
                            Response("2002", "回答成功", message1.getId());
                            String answer = data.getString("answer");
                            float score1 = data.getFloat("score1");
                            float score2 = data.getFloat("score2");
                            List<Float> score = new ArrayList<>();
                            score.add(score1);
                            score.add(score2);
                            Score.put(QuizID,score);
                            QuizAndAnswer.add(answer);
                            JSONObject back = new JSONObject();
                            back.put("answer", answer);
                            back.put("questionID", QuizID);
                            back.put("school", SchoolAndMajor.get(QuizID).get(0));
                            back.put("major", SchoolAndMajor.get(QuizID).get(1));
                            SendMessage("搜寻审核", "1010", back, Sender.get(QuizID), Respondent.get(0));
                        }
                        if (message1.getCode() == 1011) {//收到接受审核消息
                            List<String> Respondent = QuizStore.get(QuizID);
                            List<String> QuizAndAnswer = IDtoQuiz.get(QuizID);
                            int counter_check = Counter_check.get(QuizID);
                            if (Respondent.contains(message1.getId())) {
                                Response("2003", "请勿重复接受!", message1.getId());
                            }
                            if (counter_check <= 2 && !Respondent.contains(message1.getId())) {
                                if (counter_check == 1) CheckTinme1.put(QuizID, currentTime);
                                if (counter_check == 2) CheckTinme2.put(QuizID, currentTime);
                                Response("2003", "接受成功，请等待结果", message1.getId());
                                Counter_check.replace(QuizID, ++counter_check);
                                Respondent.add(message1.getId());//收集接受用户信息
                            }
                            if (counter_check == 2) {
                                JSONObject back = new JSONObject();
                                back.put("questionID", QuizID);
                                String answer = QuizAndAnswer.get(1);
                                back.put("answer", answer);
                                back.put("mark1", Mark.get(QuizID).get(0));
                                back.put("mark2", Mark.get(QuizID).get(1));
                                back.put("score1", Score.get(QuizID).get(0));
                                back.put("score2", Score.get(QuizID).get(1));
                                Response("1005", "请审核回答", back.toJSONString(), Respondent.get(1));
                                Response("1005", "请审核回答", back.toJSONString(), Respondent.get(2));
                            }
                            if (counter_check > 2) {
                                Response("2004", "手速慢了哟!", message1.getId());
                            }
                        }
                        if (message1.getCode() == 1007||message1.getCode() == 1013) {//审核结果正确或者规划师投票通过
                            List<String> Check = CheckStore.get(QuizID);
                            List<String> Respondent = QuizStore.get(QuizID);
                            List<String> QuizAndAnswer = IDtoQuiz.get(QuizID);
                            Map<String,String> reason = Reason.get(QuizID);
                            reason.put(message1.getId(),data.getString("reason"));//存储两个审核理由
                            String sender = Sender.get(QuizID);
                            int checkCounter = CheckCounter.get(QuizID);
                            if (checkCounter == 1) CheckTinme1.replace(QuizID, 1L);
                            if (checkCounter == 2) CheckTinme2.replace(QuizID, 1L);
                            if (!Check.contains(message1.getId())) {
                                Response("2005", "审核成功", message1.getId());
                                Check.add(message1.getId());
                                CheckCounter.replace(QuizID, ++checkCounter);
                            } else
                                Response("2005", "请勿重复选择", message1.getId());
                            if (checkCounter >= 2) {
                                JSONObject DATA = new JSONObject();
                                DATA.put("questionID", QuizID);
                                DATA.put("question", IDtoQuiz.get(QuizID).get(0));
                                DATA.put("answer", QuizAndAnswer.get(1));
                                DATA.put("F_id", Respondent.get(0));
                                DATA.put("S_id", Respondent.get(1));
                                DATA.put("T_id", Respondent.get(2));
                                DATA.put("mark1", Mark.get(QuizID).get(0));
                                DATA.put("mark2", Mark.get(QuizID).get(1));
                                DATA.put("score1", Score.get(QuizID).get(0));
                                DATA.put("score2", Score.get(QuizID).get(1));
                                Response("1009", "得到答案", DATA.toJSONString(), sender);
                                Quiz quiz = new Quiz(QuizID,Integer.parseInt(Sender.get(QuizID)),QuizAndAnswer.get(0),QuizAndAnswer.get(1),getNowDate(),Integer.parseInt(Respondent.get(0)),Integer.parseInt(Respondent.get(1)),Integer.parseInt(Respondent.get(2)),100,reason.get(Respondent.get(1)),reason.get(Respondent.get(2)));
                                quizService.add(quiz);
                                Remove(QuizID);
                                IsCheck.remove(QuizID);
                            }
                        }
                        if (message1.getCode() == 1008) {//审核不通过，发送投票信息到规划师
                            boolean check = IsCheck.get(QuizID);
                            String sender = Sender.get(QuizID);
                            List<String> QuizAndAnswer = IDtoQuiz.get(QuizID);
                            if (!check) {
                                JSONObject back = new JSONObject();
                                back.put("question", QuizAndAnswer.get(0));
                                back.put("answer", QuizAndAnswer.get(1));
                                back.put("questionID", QuizID);
                                back.put("toWho", "5");//指定收到消息的人为规划师
                                SendMessage("请对审核进行投票表决", "1012", back, sender);
                                IsCheck.replace(QuizID, true);
                            } else
                                IsCheck.replace(QuizID, false);
                        }
                        if (message1.getCode() == 1014) {//规划师投票不通过
                            List<String> QuizAndAnswer = IDtoQuiz.get(QuizID);
                            JSONObject back = new JSONObject();
                            back.put("question", QuizAndAnswer.get(0));
                            back.put("questionID", QuizID);
                            back.put("school", SchoolAndMajor.get(QuizID).get(0));
                            back.put("major", SchoolAndMajor.get(QuizID).get(1));
                            SendMessage("搜寻回答", "1002", back, message1.getId());
                        }
                    }
                    else
                        Response("2006", "问题已过期", message1.getId());
                }
            System.out.println("时间"+TimeStore.toString()+"提问"+QuizStore.toString()+"发送者"+Sender.toString()+"问题"+IDtoQuiz.toString());
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
    protected static void Remove(String QuizID){
        CheckStore.remove(QuizID);
        QuizStore.remove(QuizID);
        CheckCounter.remove(QuizID);
        Counter.remove(QuizID);
        IDtoQuiz.remove(QuizID);
        Sender.remove(QuizID);
        Counter_check.remove(QuizID);
        TimeStore.remove(QuizID);
        AnswerTime.remove(QuizID);
        CheckTinme1.remove(QuizID);
        CheckTinme2.remove(QuizID);
        Mark.remove(QuizID);
        Score.remove(QuizID);
        SchoolAndMajor.remove(QuizID);
        Reason.remove(QuizID);
    }
    /**
     * 获取现在时间
     *
     * @return 返回时间类型 yyyy-MM-dd HH:mm:ss
     */
    public static String getNowDate() {
        Date date = new Date();
        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        return dateFormat.format(date);
    }
    /**
     * 返回成功消息
     * @param code
     * @param message
     * @param id
     */
    private static void Response(String code,String message,String id){
        JSONObject back = new JSONObject();
        back.put("message",message);
        back.put("data","");
        back.put("code",code);
        back.put("path","");
        try {
            WebSocketServer.sendInfo(back.toJSONString(),id);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 返回消息带数据
     * @param code
     * @param message
     * @param data
     * @param id
     */
    private static void Response(String code,String message,String data,String id){
        JSONObject back = new JSONObject();
        back.put("message",message);
        back.put("data",data);
        back.put("code",code);
        back.put("path","");
        try {
            WebSocketServer.sendInfo(back.toJSONString(),id);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 向客户端发送各种类型的消息
     * @param message
     * @param code
     * @param data
     */
    private static void SendMessage(String message,String code,JSONObject data,String nid){
        JSONObject params = new JSONObject();
        params.put("message",message);
        params.put("data",data);
        params.put("code",code);
        params.put("path","");
        try {
            WebSocketServer.sendInfo(params.toJSONString(),"群发",nid);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 向客户端发送各种类型的消息
     * @param message
     * @param code
     * @param data
     */
    private static void SendMessage(String message,String code,JSONObject data,String Sid,String Rid){
        JSONObject params = new JSONObject();
        params.put("message",message);
        params.put("data",data);
        params.put("code",code);
        params.put("path","");
        try {
            WebSocketServer.sendInfo(params.toJSONString(),"群发",Sid,Rid);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void SendMessage(String message,String code,JSONObject data,List<String> nid){
        JSONObject params = new JSONObject();
        params.put("message",message);
        params.put("data",data);
        params.put("code",code);
        params.put("path","");
        try {
            WebSocketServer.sendInfo(params.toJSONString(),"群发",nid);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, Long> getAnswerTime() {
        return AnswerTime;
    }

    public static void setAnswerTime(Map<String, Long> answerTime) {
        AnswerTime = answerTime;
    }

    public static Map<String, Long> getCheckTinme1() {
        return CheckTinme1;
    }

    public static void setCheckTinme1(Map<String, Long> checkTinme1) {
        CheckTinme1 = checkTinme1;
    }

    public static Map<String, Long> getCheckTinme2() {
        return CheckTinme2;
    }

    public static void setCheckTinme2(Map<String, Long> checkTinme2) {
        CheckTinme2 = checkTinme2;
    }
    public static Map<String, List<String>> getQuizStore() {
        return QuizStore;
    }

    public static void setQuizStore(Map<String, List<String>> quizStore) {
        QuizStore = quizStore;
    }

    public static Map<String, List<String>> getCheckStore() {
        return CheckStore;
    }

    public static void setCheckStore(Map<String, List<String>> checkStore) {
        CheckStore = checkStore;
    }

    public static Map<String, Long> getTimeStore() {
        return TimeStore;
    }

    public static void setTimeStore(Map<String, Long> timeStore) {
        TimeStore = timeStore;
    }

    public static Map<String, List<String>> getIDtoQuiz() {
        return IDtoQuiz;
    }

    public static void setIDtoQuiz(Map<String, List<String>> IDtoQuiz) {
        Progress.IDtoQuiz = IDtoQuiz;
    }

    public static Map<String, Integer> getCheckCounter() {
        return CheckCounter;
    }

    public static void setCheckCounter(Map<String, Integer> checkCounter) {
        CheckCounter = checkCounter;
    }

    public static Map<String, Integer> getCounter() {
        return Counter;
    }

    public static void setCounter(Map<String, Integer> counter) {
        Counter = counter;
    }

    public static Map<String, Integer> getCounter_check() {
        return Counter_check;
    }

    public static void setCounter_check(Map<String, Integer> counter_check) {
        Counter_check = counter_check;
    }

    public static Map<String, Boolean> getIsCheck() {
        return IsCheck;
    }

    public static void setIsCheck(Map<String, Boolean> isCheck) {
        IsCheck = isCheck;
    }

    public static Map<String, String> getSender() {
        return Sender;
    }

    public static void setSender(Map<String, String> sender) {
        Sender = sender;
    }


}

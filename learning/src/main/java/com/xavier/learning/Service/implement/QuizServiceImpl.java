package com.xavier.learning.Service.implement;

import com.xavier.learning.Service.QuizService;
import com.xavier.learning.dao.quizDao;
import com.xavier.learning.entity.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class QuizServiceImpl implements QuizService {
    @Autowired
    private quizDao quizDao;
    @Override
    public Quiz getQuizById(Integer id) {
        return quizDao.getQuizById(id);
    }
    @Override
    public List<Quiz> getQuizList() {
        return quizDao.getQuizList();
    }
    @Override
    public int add(Quiz quiz) {
        return quizDao.add(quiz);
    }
    @Override
    public int update(Integer id, Quiz quiz) {
        return quizDao.update(id, quiz);
    }
    @Override
    public int delete(Integer id) {
        return quizDao.delete(id);
    }
}

package com.xavier.learning.controller;

import com.alibaba.fastjson.JSONObject;
import com.xavier.learning.WebSocket.WebSocketServer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

/**
 * Created by xiaoy on 2019/3/16 12:49
 */
@Controller
@RequestMapping("/question")
public class websocket {

    //页面请求
    @GetMapping("/socket/{cid}")
    public ModelAndView socket(@PathVariable String cid) {
        ModelAndView mav=new ModelAndView("/socket");
        mav.addObject("cid", cid);
        return mav;
    }
    //推送数据接口
    @ResponseBody
    @RequestMapping("/push/{cid}/{message}")
    public String pushToWeb(@PathVariable String cid, @PathVariable String message) {
        JSONObject params = new JSONObject();
        try {
            params.put("message",message);
            params.put("data","");
            params.put("code","1000");
            params.put("path","login");
            WebSocketServer.sendInfo(params.toJSONString(),cid,"");
        } catch (IOException e) {
            e.printStackTrace();
            return cid+"#"+e.getMessage();
        }
        return params.toJSONString();
    }
}

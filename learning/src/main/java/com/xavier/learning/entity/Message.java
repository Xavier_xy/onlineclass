package com.xavier.learning.entity;

import com.xavier.learning.entity.User.User;

/**
 * Created by xiaoy on 2019/3/17 12:38
 */
public class Message {
    private String message;
    private String data;
    private int code;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}

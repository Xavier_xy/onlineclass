package com.xavier.learning.dao.implement;

import com.xavier.learning.dao.quizDao;
import com.xavier.learning.entity.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class qiuzDaoImpl implements quizDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;  //这个是系统自带的
    @Override
    public Quiz getQuizById(Integer id) {
        List<Quiz> list = jdbcTemplate.query("select * from quiz where id = ?", new Object[]{id}, new BeanPropertyRowMapper(Quiz.class));
        if(list!=null && list.size()>0){
            return list.get(0);
        }else{
            return null;
        }
    }
    @Override
    public List<Quiz> getQuizList() {
        List<Quiz> list = jdbcTemplate.query("select * from quiz", new Object[]{}, new BeanPropertyRowMapper(Quiz.class));
        if(list!=null && list.size()>0){
            return list;
        }else{
            return null;
        }
    }

    @Override
    public int add(Quiz quiz) {
        return jdbcTemplate.update("insert into quiz(id,quiz_id, user_quiz_id, quiz,create_time,user_answer_id,answer,user_check1_id,user_check2_id,price,check_reason1,check_reason2) values(?,?,?,?,?,?,?,?,?,?,?,?)",
                quiz.getId(),quiz.getQuiz_id(),quiz.getUser_quiz_id(),quiz.getQuiz(),quiz.getCreate_time(),quiz.getUser_answer_id(),quiz.getAnswer(),quiz.getUser_check1_id(),quiz.getUser_check2_id(),quiz.getPrice(),quiz.getCheck_reason1(),quiz.getCheck_reason2());
    }

    @Override
    public int update(Integer id, Quiz quiz) {
        return jdbcTemplate.update("UPDATE quiz SET user_qiuz_id = ? , quiz = ?, create_time = ?, user_answer_id = ?, answer = ?, user_check1_id = ?, user_check2_id = ?, price = ?, check_reason1 = ?, check_reason2 = ? WHERE id=?",
                quiz.getUser_quiz_id(),quiz.getQuiz(),quiz.getCreate_time(),quiz.getUser_answer_id(),quiz.getAnswer(),quiz.getUser_check1_id(),quiz.getUser_check2_id(),quiz.getPrice(),quiz.getCheck_reason1(),quiz.getCheck_reason2(),id);
    }

    @Override
    public int delete(Integer id) {
        return jdbcTemplate.update("DELETE from quiz where id = ? ",id);
    }
}

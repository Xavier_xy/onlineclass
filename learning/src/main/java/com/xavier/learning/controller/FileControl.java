package com.xavier.learning.controller;

import com.alibaba.fastjson.JSONObject;
import com.xavier.learning.Util.FileUtil;
import com.xavier.learning.WebSocket.WebSocketServer;
import com.xavier.learning.entity.Identity;
import com.xavier.learning.entity.User.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

/**
 * Created by xiaoy on 2019/2/22 17:03
 * 文件上传方法
 */

@Controller
public class FileControl {
    @Autowired
private JdbcTemplate jdbcTemplate;
    private static Logger logger = LoggerFactory.getLogger(FileControl.class);
        //处理文件上传
         @PostMapping("/uploadimg")
         @ResponseBody
        public String uploadImg(@RequestParam("id") String id,
                         @RequestParam("file") MultipartFile file) {
            String url="";
            String contentType = file.getContentType();   //图片文件类型
            String fileName = file.getOriginalFilename();  //图片名字
            UUID uuid = UUID.randomUUID();
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));
            fileName = uuid.toString().replace("-", "")+suffix;
            //System.out.println(fileName);
            //文件存放路径
            String filePath = "C:\\imgs\\";
            //调用文件处理类FileUtil，处理文件，将文件写入指定位置
            try {
                FileUtil.uploadFile(file.getBytes(), filePath, fileName);
                //url= "http://192.168.1.106/imgs/"+fileName;
                url= "http://118.24.101.175/imgs/"+fileName;
                logger.info("文件地址："+url+" id是"+id);
                //System.out.println(url);
            } catch (Exception e) {
                // TODO: handle exception
            }

            // 返回图片的存放路径
            JSONObject data = new JSONObject();
            data.put("code",11);
            data.put("face",url);
            data.put("message","头像上传成功，地址是"+url);
             jdbcTemplate.update("UPDATE profile SET face = ?WHERE id=?",url,id);
            return data.toJSONString();
        }
    //处理文件上传
    @PostMapping("/authorize")
    @ResponseBody
    public String uploadImg(@RequestParam("id") String id,
                            @RequestParam("name") String name,
                            @RequestParam("school") String school,
                            @RequestParam("major") String major,
                            @RequestParam("identity") String identity,
                            @RequestParam("file") MultipartFile file) {
        String url="";
        JSONObject data = new JSONObject();
        String fileName = file.getOriginalFilename();  //图片名字
        UUID uuid = UUID.randomUUID();
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));//后缀名
        fileName = uuid.toString().replace("-", "")+suffix;
        //文件存放路径
        String filePath = "C:\\imgs\\";
        List<Identity> list = jdbcTemplate.query("select * from identity where id = ?", new Object[]{id}, new BeanPropertyRowMapper(Identity.class));
        if(list.size()==0) {
            //调用文件处理类FileUtil，处理文件，将文件写入指定位置
            try {
                FileUtil.uploadFile(file.getBytes(), filePath, fileName);
                //url = "http://192.168.1.106/imgs/" + fileName;
                url= "http://118.24.101.175/imgs/"+fileName;
                logger.info("文件地址：" + url + " id是" + id);
            } catch (Exception e) {

            }
            jdbcTemplate.update("insert into identity(id,name,school,major,identity,material,state) values(?,?,?,?,?,?,?)",
                    id,name, school, major, identity, url, 0);
//            jdbcTemplate.update("UPDATE identity SET school = ?,major = ?,identity = ?,material = ?,state = ? WHERE id=?",
//                    school, major, identity, url, 0, id);
            data.put("code", 21);
            data.put("message", "认证提交成功，请等待结果。");
        }
        else {
            data.put("code", 22);
            data.put("message", "已经提交认证，请等待结果。");
        }
        return data.toJSONString();
    }
}

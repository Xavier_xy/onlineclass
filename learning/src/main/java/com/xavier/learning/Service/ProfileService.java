package com.xavier.learning.Service;

import com.xavier.learning.entity.Profile;
import com.xavier.learning.entity.Quiz;

import java.util.List;

public interface ProfileService {
    Profile getProfileById(Integer id);
    Profile getProfileById(String id);
    public List<Profile> getProfileList();
    public int add(Profile profile);
    public int update(Integer id, Profile profile);
    public int delete(Integer id);
    public int updatenickname(Integer id,String nickname);
    public int updatemail(Integer id,String mail);
    public int updatesex(Integer id,String sex);
    public int updatesignature(Integer id,String signature);
    public int getstate(Integer id);
}

package com.xavier.learning.Service;

import com.xavier.learning.entity.Quiz;

import java.util.List;

public interface QuizService {
    Quiz getQuizById(Integer id);
    public List<Quiz> getQuizList();
    public int add(Quiz quiz);
    public int update(Integer id, Quiz quiz);
    public int delete(Integer id);
}

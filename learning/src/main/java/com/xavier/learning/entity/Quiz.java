package com.xavier.learning.entity;

public class Quiz {
    private int id;
    private String quiz_id;
    private int user_quiz_id;
    private String quiz;
    private String answer;
    private String create_time;
    private int user_answer_id;
    private int user_check1_id;
    private int user_check2_id;
    private int price;
    private String check_reason1;
    private String check_reason2;

    public Quiz() {
    }

    public Quiz(String quiz_id, int user_quiz_id, String quiz, String answer, String create_time, int user_answer_id, int user_check1_id, int user_check2_id, int price, String check_reason1, String check_reason2) {
        this.quiz_id = quiz_id;
        this.user_quiz_id = user_quiz_id;
        this.quiz = quiz;
        this.answer = answer;
        this.create_time = create_time;
        this.user_answer_id = user_answer_id;
        this.user_check1_id = user_check1_id;
        this.user_check2_id = user_check2_id;
        this.price = price;
        this.check_reason1 = check_reason1;
        this.check_reason2 = check_reason2;
    }

    public String getCheck_reason1() {
        return check_reason1;
    }

    public void setCheck_reason1(String check_reason1) {
        this.check_reason1 = check_reason1;
    }

    public String getCheck_reason2() {
        return check_reason2;
    }

    public void setCheck_reason2(String check_reason2) {
        this.check_reason2 = check_reason2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(String quiz_id) {
        this.quiz_id = quiz_id;
    }

    public int getUser_quiz_id() {
        return user_quiz_id;
    }

    public void setUser_quiz_id(int user_quiz_id) {
        this.user_quiz_id = user_quiz_id;
    }

    public String getQuiz() {
        return quiz;
    }

    public void setQuiz(String quiz) {
        this.quiz = quiz;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public int getUser_answer_id() {
        return user_answer_id;
    }

    public void setUser_answer_id(int user_answer_id) {
        this.user_answer_id = user_answer_id;
    }

    public int getUser_check1_id() {
        return user_check1_id;
    }

    public void setUser_check1_id(int user_check1_id) {
        this.user_check1_id = user_check1_id;
    }

    public int getUser_check2_id() {
        return user_check2_id;
    }

    public void setUser_check2_id(int user_check2_id) {
        this.user_check2_id = user_check2_id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

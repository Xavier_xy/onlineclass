package com.xavier.learning.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.xavier.learning.entity.Profile;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("TokenService")
public class TokenService {
    private int time = 24*60;//token有效时间，60分钟
    public String getToken(Profile profile) {
        String token="";
        token= JWT.create().withAudience(profile.getAccount())// 将 user id 保存到 token 里面
                .withIssuedAt(new Date(System.currentTimeMillis())) //生成签名的时间
		        .withExpiresAt(new Date(System.currentTimeMillis()+time*60*1000L))//签名过期的时间
                .sign(Algorithm.HMAC256(profile.getPassword()));// 以 password 作为 token 的密钥
        return token;
    }
}

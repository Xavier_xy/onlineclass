package com.xavier.learning.WebSocket;

import com.alibaba.fastjson.JSON;
import com.xavier.learning.Quiz.Progress;
import com.xavier.learning.Service.ProfileService;
import com.xavier.learning.Service.QuizService;
import com.xavier.learning.entity.Message;
import com.xavier.learning.entity.Profile;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;
import com.alibaba.fastjson.JSONObject;

@Slf4j
@ServerEndpoint(value = "/xavier/websocket/{sid}")
@Component
public class WebSocketServer {

    private static Logger logger = LoggerFactory.getLogger(WebSocketServer.class);
    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static int onlineCount = 0;
    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    private static CopyOnWriteArraySet<WebSocketServer> webSocketSet = new CopyOnWriteArraySet<WebSocketServer>();

    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    //此处是解决无法注入的关键
    private static ApplicationContext applicationContext;
    //要注入的service或者dao
    private ProfileService profileService;
    private QuizService quizService;
    public static void setApplicationContext(ApplicationContext applicationContext) {
        WebSocketServer.applicationContext = applicationContext;
    }

    //接收sid
    private String sid="";
    /**
     * 连接建立成功调用的方法*/
    @OnOpen
    public void onOpen(Session session, @PathParam("sid") String id){
        profileService = applicationContext.getBean(ProfileService.class);
        sid=id;
        this.session = session;
        webSocketSet.add(this);     //加入set中
        addOnlineCount();           //在线数加1
        Integer Id = new Integer(id);
        Profile profile = profileService.getProfileById(Id);
        int state = profileService.getstate(Id);
        JSONObject params = new JSONObject();
        params.put("message","连接成功");
        params.put("data",profile.toString());
        params.put("code","1000");
        params.put("path","login");
        params.put("state",state);

        logger.info("有新连接加入！当前在线人数为" + getOnlineCount()+",id是"+sid);
        //System.out.println("有新连接加入！当前在线人数为" + getOnlineCount()+",id是"+sid+",身份是："+Identity);
        try {
            sendMessage(params.toJSONString());
        } catch (IOException e) {
            logger.info("IO异常");
           // System.out.println("IO异常");
        }
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        webSocketSet.remove(this);  //从set中删除
        subOnlineCount();           //在线数减1
        logger.info("有一连接关闭！当前在线人数为" + getOnlineCount());
        //System.out.println("有一连接关闭！当前在线人数为" + getOnlineCount());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息*/
    @OnMessage
    public void onMessage(String message, Session session) {
        profileService = applicationContext.getBean(ProfileService.class);
        quizService = applicationContext.getBean(QuizService.class);
        logger.info("来自客户端的消息:" + message);
        //System.out.println("来自客户端的消息:" + message);
        Message message1 = JSON.parseObject(message, Message.class);
        JSONObject data = JSON.parseObject(message1.getData());
        JSONObject msg = JSON.parseObject(message);
        int code = message1.getCode();
        if(code>999)
        Progress.SolveMessage(message,quizService);
        else if(code==101)
            {
                int id = msg.getInteger("id");
                profileService.updatenickname(id,data.getString("nickname"));
                JSONObject params = new JSONObject();
                params.put("message","昵称设置成功");
                params.put("data",data.getString("nickname"));
                params.put("code","201");
                try {
                    sendInfo(params.toJSONString(),message1.getId());
                } catch (IOException e) {
                    logger.info("IO异常");
                }
            }
            else if(code==102)
            {
                int id = msg.getInteger("id");
                profileService.updatemail(id,data.getString("mail"));
                JSONObject params = new JSONObject();
                params.put("message","邮件设置成功");
                params.put("data",data.getString("mail"));
                params.put("code","202");
                try {
                    sendInfo(params.toJSONString(),message1.getId());
                } catch (IOException e) {
                    logger.info("IO异常");
                }
            }
            else if(code==103)
            {
                int id = msg.getInteger("id");
                profileService.updatesex(id,data.getString("sex"));
                JSONObject params = new JSONObject();
                params.put("message","性别设置成功");
                params.put("data",data.getString("sex"));
                params.put("code","203");
                try {
                    sendInfo(params.toJSONString(),message1.getId());
                } catch (IOException e) {
                    logger.info("IO异常");
                }
            }
            else if(code==104)
            {
                int id = msg.getInteger("id");
                profileService.updatesignature(id,data.getString("signature"));
                JSONObject params = new JSONObject();
                params.put("message","个性签名设置成功");
                params.put("data",data.getString("signature"));
                params.put("code","204");
                try {
                    sendInfo(params.toJSONString(),message1.getId());
                } catch (IOException e) {
                    logger.info("IO异常");
                }
            }

    }

    /**
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        logger.info("发生错误");
       // System.out.println("发生错误");
        error.printStackTrace();
    }


    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }


    /**
     * 群发自定义消息
     * */
    public static void sendInfo(String message,String sid) throws IOException {
        logger.info("推送消息到窗口"+sid+"，推送内容:"+message);
       // System.out.println("推送消息到窗口"+sid+"，推送内容:"+message);
        for (WebSocketServer item : webSocketSet) {
            try {
                //这里可以设定只推送给这个sid的，为null则全部推送
                if(sid.equals("群发")) {
                    item.sendMessage(message);
                }else if(item.sid.equals(sid)){
                    item.sendMessage(message);
                }
            } catch (IOException e) {
                continue;
            }
        }
    }
    /**
     * 群发自定义消息,不发送给提问或者回答方
     * */
    public static void sendInfo(String message,String sid,String nid) throws IOException {
        logger.info("推送消息到窗口"+sid+"，推送内容:"+message);
        //System.out.println("推送消息到窗口"+sid+"，推送内容:"+message);
        for (WebSocketServer item : webSocketSet) {
            try {
                //这里可以设定只推送给这个sid的，为null则全部推送
                if(sid.equals("群发")) {
                    if(!item.sid.equals(nid))
                        item.sendMessage(message);
                }else if(item.sid.equals(sid)){
                    item.sendMessage(message);
                }
            } catch (IOException e) {
                continue;
            }
        }
    }
    public static void sendInfo(String message, String sid, List<String> nid) throws IOException {
        logger.info("推送消息到窗口"+sid+"，推送内容:"+message);
        //System.out.println("推送消息到窗口"+sid+"，推送内容:"+message);
        for (WebSocketServer item : webSocketSet) {
            try {
                //这里可以设定只推送给这个sid的，为null则全部推送
                if(sid.equals("群发")) {
                    if(!nid.contains(item.sid))
                        item.sendMessage(message);
                }else if(item.sid.equals(sid)){
                    item.sendMessage(message);
                }
            } catch (IOException e) {
                continue;
            }
        }
    }
    /**
     * 群发自定义消息,不发送给提问和回答方
     * */
    public static void sendInfo(String message,String sid,String Sid,String Rid) throws IOException {
        logger.info("推送消息到窗口"+sid+"，推送内容:"+message);
        //System.out.println("推送消息到窗口"+sid+"，推送内容:"+message);
        for (WebSocketServer item : webSocketSet) {
            try {
                //这里可以设定只推送给这个sid的，为null则全部推送
                if(sid.equals("群发")) {
                    if(!item.sid.equals(Sid)&&!item.sid.equals(Rid))
                        item.sendMessage(message);
                }else if(item.sid.equals(sid)){
                    item.sendMessage(message);
                }
            } catch (IOException e) {
                continue;
            }
        }
    }
    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketServer.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketServer.onlineCount--;
    }
}
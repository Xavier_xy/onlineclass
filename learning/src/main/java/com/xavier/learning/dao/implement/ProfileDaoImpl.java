package com.xavier.learning.dao.implement;

import com.xavier.learning.dao.ProfileDao;
import com.xavier.learning.dao.quizDao;
import com.xavier.learning.entity.Identity;
import com.xavier.learning.entity.Profile;
import com.xavier.learning.entity.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProfileDaoImpl implements ProfileDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;  //这个是系统自带的
    @Override
    public Profile getProfileById(Integer id) {
        List<Profile> list = jdbcTemplate.query("select * from profile where id = ?", new Object[]{id}, new BeanPropertyRowMapper(Profile.class));
        if(list!=null && list.size()>0){
            return list.get(0);
        }else{
            return null;
        }
    }
    @Override
    public Profile getProfileById(String id) {
        List<Profile> list = jdbcTemplate.query("select * from profile where account = ?", new Object[]{id}, new BeanPropertyRowMapper(Profile.class));
        if(list!=null && list.size()>0){
            return list.get(0);
        }else{
            return null;
        }
    }

    @Override
    public int updatenickname(Integer id, String nickname) {
        return jdbcTemplate.update("UPDATE profile SET nickname = ? WHERE id=?",
                nickname,id);
    }

    @Override
    public int updatemail(Integer id, String mail) {
        return jdbcTemplate.update("UPDATE profile SET mail = ? WHERE id=?",
                mail,id);
    }

    @Override
    public int updatesex(Integer id, String sex) {
        return jdbcTemplate.update("UPDATE profile SET sex = ? WHERE id=?",
                sex,id);
    }

    @Override
    public int updatesignature(Integer id, String signature) {
        return jdbcTemplate.update("UPDATE profile SET signature = ? WHERE id=?",
                signature,id);
    }

    @Override
    public List<Profile> getProfileList() {
        List<Profile> list = jdbcTemplate.query("select * from profile", new Object[]{}, new BeanPropertyRowMapper(Profile.class));
        if(list!=null && list.size()>0){
            return list;
        }else{
            return null;
        }
    }

    @Override
    public int add(Profile profile) {
        return jdbcTemplate.update("insert into profile(id,face,nickname,mail,sex,signature) values(?,?,?,?,?,?)",
                profile.getId(),profile.getFace(),profile.getNickname(),profile.getMail(),profile.getSex(),profile.getSignature());
    }

    @Override
    public int update(Integer id, Profile profile) {
        return jdbcTemplate.update("UPDATE profile SET id = ? , face = ?, nickname = ?, mail = ?, sex = ?, signature = ? WHERE id=?",
                profile.getId(),profile.getFace(),profile.getNickname(),profile.getMail(),profile.getSex(),profile.getSignature(),id);
    }

    @Override
    public int delete(Integer id) {
        return jdbcTemplate.update("DELETE from profile where id = ? ",id);
    }

    @Override
    public int getstate(Integer id) {
        List<Identity> list = jdbcTemplate.query("select * from identity where id = ?", new Object[]{id}, new BeanPropertyRowMapper(Identity.class));
        if(list!=null&&list.size()>0)
        return list.get(0).getState();
        else
            return 10;
    }
}

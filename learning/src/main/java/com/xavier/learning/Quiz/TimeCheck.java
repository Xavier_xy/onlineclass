package com.xavier.learning.Quiz;

import com.alibaba.fastjson.JSONObject;
import com.xavier.learning.controller.FileControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by xiaoy on 2019/4/23 23:37
 * 检查提问所涉及的时间是否超时
 */
@Component
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务

public class TimeCheck {
    private static Logger logger = LoggerFactory.getLogger(TimeCheck.class);
        //3.添加定时任务
//        @Scheduled(cron = "0/5 * * * * ?")
        //或直接指定时间间隔，例如：5秒
        @Scheduled(fixedRate=60000)
        private void configureTasks() {
            Long currentTime = System.currentTimeMillis();
//            System.out.println("现在时间"+currentTime);
            for (Map.Entry<String, Long> entry : Progress.getTimeStore().entrySet()) {
                String QiuzID = entry.getKey();
//                System.out.println((entry.getValue()-currentTime)/1000);
                if ((currentTime-entry.getValue())/1000>600){
                    Progress.Remove(QiuzID);
                    Progress.getIsCheck().remove(QiuzID);
                    logger.info("问题"+QiuzID+"已过期");
                    //System.out.println("问题"+QiuzID+"已过期");
                }
            }
                for (Map.Entry<String, Long> entry : Progress.getAnswerTime().entrySet()) {
                    String QiuzID = entry.getKey();
                if ((currentTime-entry.getValue())/1000>600&&entry.getValue()!=0&&entry.getValue()!=1){//回答大于十分钟
                    JSONObject back = new JSONObject();
                    back.put("question",Progress.getIDtoQuiz().get(QiuzID) );
                    back.put("questionID",QiuzID);
                    List<String> Nid = new ArrayList<>();
                    Nid.add(Progress.getQuizStore().get(QiuzID).get(0));
                    Nid.add(Progress.getSender().get(QiuzID));
                    Progress.SendMessage("搜寻回答", "1002", back,Nid);//重新搜寻回答
                    Progress.getCounter().replace(QiuzID,0);
                    Progress.getQuizStore().get(QiuzID).clear();
                    entry.setValue(0L);
                }
            }
            for (Map.Entry<String, Long> entry : Progress.getCheckTinme1().entrySet()) {
                String QiuzID = entry.getKey();
                if ((currentTime-entry.getValue())/1000>600&&entry.getValue()!=0&&entry.getValue()!=1){//审核人1大于十分钟
                    JSONObject back = new JSONObject();
                    back.put("answer", Progress.getIDtoQuiz().get(QiuzID).get(1));
                    back.put("questionID", QiuzID);
                    List<String> Nid = new ArrayList<>();
                    Nid.add(Progress.getQuizStore().get(QiuzID).get(0));
                    Nid.add(Progress.getQuizStore().get(QiuzID).get(1));
                    Nid.add(Progress.getSender().get(QiuzID));
                    Progress.SendMessage("搜寻审核", "1010", back,Nid);
                    Progress.getCheckCounter().replace(QiuzID,1);
                    Progress.getQuizStore().get(QiuzID).remove(1);//取消原有的第一审核者
                    entry.setValue(0L);
                }
            }
            for (Map.Entry<String, Long> entry : Progress.getAnswerTime().entrySet()) {
                String QiuzID = entry.getKey();
                if ((currentTime-entry.getValue())/1000>600&&entry.getValue()!=0&&entry.getValue()!=1){//审核人2大于十分钟
                    JSONObject back = new JSONObject();
                    back.put("answer", Progress.getIDtoQuiz().get(QiuzID).get(1));
                    back.put("questionID", QiuzID);
                    List<String> Nid = new ArrayList<>();
                    Nid.add(Progress.getQuizStore().get(QiuzID).get(0));
                    Nid.add(Progress.getQuizStore().get(QiuzID).get(1));
                    Nid.add(Progress.getQuizStore().get(QiuzID).get(2));
                    Nid.add(Progress.getSender().get(QiuzID));
                    Progress.SendMessage("搜寻审核", "1010", back,Nid);
                    Progress.getCheckCounter().replace(QiuzID,1);
                    Progress.getQuizStore().get(QiuzID).remove(2);//取消原有的第二审核者
                    entry.setValue(0L);
                }
            }
        }

}

package com.xavier.learning.controller;
/**

  * @author oyc

  * @Description: 用户控制类

  * @date 2018/7/8 22:10

  */

import com.xavier.learning.Service.ProfileService;
import com.xavier.learning.Service.TokenService;
import com.xavier.learning.annotation.UserLoginToken;
import com.xavier.learning.entity.Profile;
import com.xavier.learning.entity.User.User;
import com.xavier.learning.entity.User.UserJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserControl {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    TokenService tokenService;
    @Autowired
    ProfileService profileService;
    /**
     * 登录
     * @param account
     * @param password
     * @return
     */
    @RequestMapping(value = "/login/{account}/{password}", method = RequestMethod.GET)
    public UserJson Login(@PathVariable(value = "account") String account
    ,@PathVariable(value = "password") String password) {
        List<Profile> list = jdbcTemplate.query("select * from profile where account = ?", new Object[]{account}, new BeanPropertyRowMapper(Profile.class));
        int result = list.size();
        if(result==1) {
            Profile profile = list.get(0);
            if (profile.getPassword().equals(password)) {
                UserJson userJson = new UserJson();
                userJson.setCode(11);
                userJson.setBody(profile);
                userJson.setMessage("Login Success!");
                int state = profileService.getstate(profile.getId());
                userJson.setState(state);
                String token = tokenService.getToken(profile);
                userJson.setToken(token);
                return userJson;

            } else {
                UserJson userJson = new UserJson();
                userJson.setCode(12);
                    userJson.setMessage("Login Failed,Wrong Password!");
                    return userJson;
            }
        }
        else {
            UserJson userJson = new UserJson();
            userJson.setCode(13);
            userJson.setMessage("Login Failed,Account Does Not Exist!");
            return userJson;
        }
    }
    @UserLoginToken
    @GetMapping("/login/token/{id}")
    public UserJson getMessage(@PathVariable(value = "id") String id){
        UserJson userJson = new UserJson();
        userJson.setCode(20);
        int state = profileService.getstate(Integer.valueOf(id));
        userJson.setState(state);
        userJson.setMessage("Token Login Success");
        return userJson;
    }
    /**
     * 根据id查找用户
     * @param id
     * @return
     */
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public UserJson findUsers(@PathVariable(value = "id") Integer id) {
        List<User> list = jdbcTemplate.query("select * from User where id = ?", new Object[]{id}, new BeanPropertyRowMapper(User.class));
        User user = list.get(0);
        UserJson userJson=new UserJson();
        userJson.setCode(1);
        userJson.setMessage("Find Success!");
        userJson.setBody(user);
        return userJson;
    }

    /**
     * 查找全部的用户
     * @return
     */
    @GetMapping("/getAll")
    public UserJson getUserList() {
        List<User> list = jdbcTemplate.query("select * from User", new Object[]{}, new BeanPropertyRowMapper(User.class));
        if (list != null && list.size() > 0) {
            UserJson userJson=new UserJson();
            userJson.setCode(2);
            userJson.setMessage("Find All Users Success!");
            userJson.setBody(list);
            return userJson;
        } else {
            return null;
        }
    }

    /**
     * 增加用户
     * @param account
     * @param password
     * @return
     */
    @PostMapping("/addUser")
    public UserJson userRegister(@RequestParam("account") String account,
                                 @RequestParam("password") String password){
        Profile profile = new Profile();
        profile.setAccount(account);
        profile.setPassword(password);
        List<Profile> list = jdbcTemplate.query("select * from profile where account = ?", new Object[]{account}, new BeanPropertyRowMapper(Profile.class));
        int result = list.size();
        if(result==0) {
            jdbcTemplate.update("insert into profile(id,account,password) values(?,?,?)",
                    profile.getId(), profile.getAccount(), profile.getPassword());
            UserJson userJson = new UserJson();
            userJson.setCode(3);
            userJson.setMessage("And User Success!");
            userJson.setBody(profile);
            return userJson;
        }
        else {
            UserJson userJson = new UserJson();
            userJson.setCode(5);
            userJson.setMessage("User Already Exist!");
            return userJson;
        }
    }
    @PostMapping("/addQQUser")
    public UserJson QQ_userRegister(@RequestParam("openid") String openid,
                                 @RequestParam("nickname") String nickname,
                                 @RequestParam("gender") String gender){
        List<User> list = jdbcTemplate.query("select * from qquser where openid = ?", new Object[]{openid}, new BeanPropertyRowMapper(User.class));
        int result = list.size();
        if(result==0) {
            jdbcTemplate.update("insert into qquser(openid,nickname, gender) values(?,?,?)",
                    openid, nickname, gender);
            UserJson userJson = new UserJson();
            userJson.setCode(8);
            userJson.setMessage("And QQUser Success!");
            return userJson;
        }
        else {
            UserJson userJson = new UserJson();
            userJson.setCode(9);
            userJson.setMessage("User Already Exist!");
            return userJson;
        }
    }
    /**
     * 根据account删除用户
     * @param account
     * @return
     */
    @RequestMapping(value = "/delete/{account}", method = RequestMethod.DELETE)
    public UserJson delete(@PathVariable(value = "account") String account){
            Integer result = jdbcTemplate.update("DELETE from User where account = ? ",account);
        UserJson userJson=new UserJson();
        userJson.setCode(6);
        if(result==1)
            userJson.setMessage("Delete Success!");
        else
            userJson.setMessage("Delete Failed!");
        return userJson;
    }
    @PostMapping("/updateUser")
    public UserJson update(@RequestParam("account") String account,
                           @RequestParam("password") String password) {
        int result = jdbcTemplate.update("UPDATE User SET password = ? WHERE account=?",
                password, account);
        UserJson userJson=new UserJson();
        userJson.setCode(7);
        if(result==1)
            userJson.setMessage("Update Success!");
        else
            userJson.setMessage("Update Failed!");
        return userJson;
    }
}
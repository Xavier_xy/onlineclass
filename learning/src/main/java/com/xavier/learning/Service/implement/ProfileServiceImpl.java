package com.xavier.learning.Service.implement;

import com.xavier.learning.Service.ProfileService;
import com.xavier.learning.Service.QuizService;
import com.xavier.learning.dao.ProfileDao;
import com.xavier.learning.dao.quizDao;
import com.xavier.learning.entity.Profile;
import com.xavier.learning.entity.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileServiceImpl implements ProfileService {
    @Autowired
    private ProfileDao profileDao;
    @Override
    public Profile getProfileById(Integer id) {
        return profileDao.getProfileById(id);
    }
    @Override
    public Profile getProfileById(String id) {
        return profileDao.getProfileById(id);
    }
    @Override
    public List<Profile> getProfileList() {
        return profileDao.getProfileList();
    }
    @Override
    public int add(Profile profile) {
        return profileDao.add(profile);
    }
    @Override
    public int update(Integer id, Profile profile) {
        return profileDao.update(id, profile);
    }
    @Override
    public int delete(Integer id) {
        return profileDao.delete(id);
    }

    @Override
    public int updatenickname(Integer id, String nickname) {
        return profileDao.updatenickname(id, nickname);
    }

    @Override
    public int updatemail(Integer id, String mail) {
        return profileDao.updatemail(id, mail);
    }

    @Override
    public int updatesex(Integer id, String sex) {
        return profileDao.updatesex(id, sex);
    }

    @Override
    public int updatesignature(Integer id, String signature) {
        return profileDao.updatesignature(id, signature);
    }

    @Override
    public int getstate(Integer id) {
        return profileDao.getstate(id);
    }
}

package com.xavier.learning.controller;

import com.xavier.learning.Service.ProfileService;
import com.xavier.learning.Service.QuizService;
import com.xavier.learning.entity.JsonResult;
import com.xavier.learning.entity.Profile;
import com.xavier.learning.entity.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProfileController {
    @Autowired
    private ProfileService profileService;
    /**
     * 根据ID查询用户
     * @param id
     * @return
     */
    @RequestMapping(value = "profile/{id}", method = RequestMethod.GET)
    public ResponseEntity<JsonResult> getQuizById (@PathVariable(value = "id") Integer id){
        JsonResult r = new JsonResult();
        try {
            Profile profile = profileService.getProfileById(id);
            r.setResult(profile);
            r.setStatus("ok");
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");
            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }

    /**
     * 查询用户列表
     * @return
     */
    @RequestMapping(value = "profiles", method = RequestMethod.GET)
    public ResponseEntity<JsonResult> getQuizList (){
        JsonResult r = new JsonResult();
        try {
            List<Profile> profiles = profileService.getProfileList();
            r.setResult(profiles);
            r.setStatus("ok");
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");
            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }

    /**
     * 添加用户
     * @param profile
     * @return

     * @requestBody注解常用来处理content-type不是默认的application/x-www-form-urlcoded编码的内容，

     * 比如说：application/json或者是application/xml等。一般情况下来说常用其来处理application/json类型。

     *   就是从json中提取数据  参见：https://www.cnblogs.com/qiankun-site/p/5774300.html
     */
    @RequestMapping(value = "profile", method = RequestMethod.POST)
    public ResponseEntity<JsonResult> add (@RequestBody Profile profile){
        JsonResult r = new JsonResult();
        try {
            int orderId = profileService.add(profile);
            if (orderId < 0) {
                r.setResult(orderId);
                r.setStatus("fail");
            } else {
                r.setResult(orderId);
                r.setStatus("ok");
            }
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");

            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }

    /**
     * 根据id删除用户
     * @param id
     * @return
     */
    @RequestMapping(value = "profile/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<JsonResult> delete (@PathVariable(value = "id") Integer id){
        JsonResult r = new JsonResult();
        try {
            int ret = profileService.delete(id);
            if (ret < 0) {
                r.setResult(ret);
                r.setStatus("fail");
            } else {
                r.setResult(ret);
                r.setStatus("ok");
            }
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");

            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }

    /**
     * 根据id修改用户信息
     * @param profile
     * @return
     */
    @RequestMapping(value = "profile/{id}", method = RequestMethod.PUT)
    public ResponseEntity<JsonResult> update (@PathVariable("id") Integer id, @RequestBody Profile profile){
        JsonResult r = new JsonResult();
        try {
            int ret = profileService.update(id, profile);
            if (ret < 0) {
                r.setResult(ret);
                r.setStatus("fail");
            } else {
                r.setResult(ret);
                r.setStatus("ok");
            }
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");

            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }
}

package com.xavier.learning.controller;

import com.alibaba.fastjson.JSONObject;
import com.xavier.learning.Service.ProfileService;
import com.xavier.learning.Service.QuizService;
import com.xavier.learning.entity.JsonResult;
import com.xavier.learning.entity.Profile;
import com.xavier.learning.entity.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class QuizController {
    @Autowired
    private ProfileService profileService;
    @Autowired
    private QuizService quizService;
    /**
     * 根据ID查询用户
     * @param id
     * @return
     */
    @RequestMapping(value = "quiz/{id}", method = RequestMethod.GET)
    public ResponseEntity<JsonResult> getQuizById (@PathVariable(value = "id") Integer id){
        JsonResult r = new JsonResult();
        try {
            Quiz quiz = quizService.getQuizById(id);
            r.setResult(quiz);
            r.setStatus("ok");
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");
            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }
    @RequestMapping(value = "getquizs", method = RequestMethod.GET)
    public ResponseEntity<JsonResult> getQuizById (){
        JsonResult r = new JsonResult();
        try {
            List<JSONObject> datas = new ArrayList<>();
            List<Quiz> quizs = quizService.getQuizList();
            for(int i=0;i<quizs.size();i++){
                Quiz Quizs = quizs.get(i);
                String quiz = Quizs.getQuiz();
                String answer = Quizs.getAnswer();
                JSONObject data = new JSONObject();
                data.put("quiz",quiz);
                data.put("answer",answer);
                data.put("quiz_face",profileService.getProfileById(Quizs.getUser_quiz_id()).getFace());
                data.put("quiz_nickname",profileService.getProfileById(Quizs.getUser_quiz_id()).getNickname());
                data.put("answer_face",profileService.getProfileById(Quizs.getUser_answer_id()).getFace());
                data.put("answer_nickname",profileService.getProfileById(Quizs.getUser_answer_id()).getNickname());
                data.put("quiz_id",Quizs.getUser_quiz_id());
                data.put("answer_id",Quizs.getUser_answer_id());
                data.put("check1_id",Quizs.getUser_check1_id());
                data.put("check2_id",Quizs.getUser_check2_id());
                data.put("CreatTime",Quizs.getCreate_time());
                datas.add(data);
            }
            r.setResult(datas);
            r.setStatus("ok");
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");
            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }

    /**
     * 查询用户列表
     * @return
     */
    @RequestMapping(value = "quizs", method = RequestMethod.GET)
    public ResponseEntity<JsonResult> getQuizList (){
        JsonResult r = new JsonResult();
        try {
            List<Quiz> quizs = quizService.getQuizList();
            r.setResult(quizs);
            r.setStatus("ok");
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");
            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }

    /**
     * 添加用户
     * @param quiz
     * @return

     * @requestBody注解常用来处理content-type不是默认的application/x-www-form-urlcoded编码的内容，

     * 比如说：application/json或者是application/xml等。一般情况下来说常用其来处理application/json类型。

     *   就是从json中提取数据  参见：https://www.cnblogs.com/qiankun-site/p/5774300.html
     */
    @RequestMapping(value = "quiz", method = RequestMethod.POST)
    public ResponseEntity<JsonResult> add (@RequestBody Quiz quiz){
        JsonResult r = new JsonResult();
        try {
            int orderId = quizService.add(quiz);
            if (orderId < 0) {
                r.setResult(orderId);
                r.setStatus("fail");
            } else {
                r.setResult(orderId);
                r.setStatus("ok");
            }
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");

            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }

    /**
     * 根据id删除用户
     * @param id
     * @return
     */
    @RequestMapping(value = "quiz/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<JsonResult> delete (@PathVariable(value = "id") Integer id){
        JsonResult r = new JsonResult();
        try {
            int ret = quizService.delete(id);
            if (ret < 0) {
                r.setResult(ret);
                r.setStatus("fail");
            } else {
                r.setResult(ret);
                r.setStatus("ok");
            }
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");

            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }

    /**
     * 根据id修改用户信息
     * @param quiz
     * @return
     */
    @RequestMapping(value = "quiz/{id}", method = RequestMethod.PUT)
    public ResponseEntity<JsonResult> update (@PathVariable("id") Integer id, @RequestBody Quiz quiz){
        JsonResult r = new JsonResult();
        try {
            int ret = quizService.update(id, quiz);
            if (ret < 0) {
                r.setResult(ret);
                r.setStatus("fail");
            } else {
                r.setResult(ret);
                r.setStatus("ok");
            }
        } catch (Exception e) {
            r.setResult(e.getClass().getName() + ":" + e.getMessage());
            r.setStatus("error");

            e.printStackTrace();
        }
        return ResponseEntity.ok(r);
    }
}
